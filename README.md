# Transactions

Transaction is a JVM library that helps execute multiple actions which need to be reverted if one of them fails.

## How to get it

1. Add the projects GitLab repository to your build.gradle.kts:

```kotlin
repositories {
    // ...
    maven {
        url = uri("https://gitlab.com/api/v4/projects/25105506/packages/maven")
    }
    // ...
}
```

2. Add the dependency:

````kotlin
dependencies {
    // ...
    implementation("at.mindcloud.transactions:transactions:0.0.1-experimental")
    // ...
}
````

## How to use it

### transaction

````kotlin
import at.mindcloud.transactions.transaction

fun main() {
    var count = 0
    val transaction = transaction<Long> {
        val resultA = action(
            execute = { count += 1 },
            revert = { count -= 1 },
        )
        val resultB = action(
            execute = { count += 2 },
            revert = { count -= 2 },
        )
        resultB
    }
    val result = transaction.execute()
    println(result)
}
````

### transactional

````kotlin
import at.mindcloud.transactions.transactional

fun main() {
    var count = 0
    val result = transactional<Long> {
        val resultA = action(
            execute = { count += 1 },
            revert = { count -= 1 },
        )
        val resultB = action(
            execute = { count += 2 },
            revert = { count -= 2 },
        )
        resultB
    }
    println(result)
}
````

### Example Project

A tiny and simple example project can be found [here](https://gitlab.com/AlexBiskup/transactions-example).
