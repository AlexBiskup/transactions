package at.mindcloud.retry

fun <T> retry(maxAttempts: Int = 2, block: RetryContext.() -> T): T {

    val context = MutableRetryContext(maxAttempts)

    repeat(times = maxAttempts) { i ->
        try {
            context.attempt = i + 1
            return block(context)
        } catch (exception: Exception) {
            context.retryCause = exception
        }
    }
    throw context.retryCause!!
}

interface RetryContext {

    val attempt: Int

    val maxAttempts: Int

    val retryCause: Exception?

}

fun RetryContext.isFirstAttempt(): Boolean = attempt == 1

fun RetryContext.isNotFirstAttempt(): Boolean = !isFirstAttempt()

fun RetryContext.isLastAttempt(): Boolean = attempt == maxAttempts

fun RetryContext.isNotLastAttempt() = !isLastAttempt()


private class MutableRetryContext(override val maxAttempts: Int) : RetryContext {

    override var attempt: Int = 1

    override var retryCause: Exception? = null

}
