package at.mindcloud.cache

import java.time.Duration
import java.time.LocalDateTime
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

object CacheDelegates {

    fun <T, V> cache(value: V, expirationDate: LocalDateTime = LocalDateTime.MAX): ReadOnlyProperty<T, V> = CacheDelegate(value, expirationDate)

    fun <T, V> cache(value: V, expirationDuration: Duration): ReadOnlyProperty<T, V> {
        return CacheDelegate(value, expirationDate = LocalDateTime.now().plus(expirationDuration))
    }

    fun <T, V> refreshableCache(value: V, expirationDuration: Duration): ReadWriteProperty<T, V> = RefreshableCacheDelegate(value, expirationDuration)

    fun <T, V> refreshingCache(expirationDuration: Duration, refresh: () -> V): ReadWriteProperty<T, V> {
        return RefreshingCacheDelegate(expirationDuration, refresh)
    }

}

private class CacheDelegate<T, V>(value: V, expirationDate: LocalDateTime) : ReadOnlyProperty<T, V> {

    private val cache = cache(value, expirationDate)

    override fun getValue(thisRef: T, property: KProperty<*>): V {
        return cache.get()
    }

}

private class RefreshableCacheDelegate<T, V>(initialValue: V, expirationDuration: Duration) : ReadWriteProperty<T, V> {

    private val cache = refreshableCache(initialValue, expirationDuration)

    override fun setValue(thisRef: T, property: KProperty<*>, value: V) {
        cache.refresh(value)
    }

    override fun getValue(thisRef: T, property: KProperty<*>): V {
        return cache.get()
    }

}

private class RefreshingCacheDelegate<T, V>(expirationDuration: Duration, refresh: () -> V) : ReadWriteProperty<T, V> {

    private val cache = refreshingCache(expirationDuration, refresh)

    override fun setValue(thisRef: T, property: KProperty<*>, value: V) {
        cache.refresh(value)
    }

    override fun getValue(thisRef: T, property: KProperty<*>): V {
        return cache.get()
    }

}
