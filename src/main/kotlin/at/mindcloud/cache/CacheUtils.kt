package at.mindcloud.cache

import java.time.Duration
import java.time.LocalDateTime

fun <T> cache(value: T, expirationDate: LocalDateTime = LocalDateTime.MAX): Cache<T> = MutableCacheImpl(value, expirationDate)

fun <T> cache(value: T, expirationDuration: Duration): Cache<T> {
    return MutableCacheImpl(value, expirationDate = LocalDateTime.now().plus(expirationDuration))
}

fun <T> refreshableCache(initialValue: T, expirationDuration: Duration): RefreshableCache<T> = RefreshableCacheImpl(initialValue, expirationDuration)

fun <T> refreshingCache(expirationDuration: Duration, refresh: () -> T): RefreshableCache<T> = RefreshingCache(expirationDuration, refresh)


interface Cache<out T> {

    fun get(): T

    fun invalidate()

    fun isValid(): Boolean

    fun hasExpired(): Boolean = !isValid()

    val expirationDate: LocalDateTime

}

interface RefreshableCache<T> : Cache<T> {

    fun refresh(value: T)

}

class InvalidCacheException : IllegalStateException()

private open class MutableCacheImpl<T>(var value: T, final override var expirationDate: LocalDateTime) : Cache<T> {

    final override fun get(): T {
        if (hasExpired()) throw InvalidCacheException()
        return value
    }

    final override fun invalidate() {
        expirationDate = LocalDateTime.now()
    }

    final override fun isValid(): Boolean {
        return LocalDateTime.now().isBefore(expirationDate)
    }

}

private class RefreshableCacheImpl<T>(
    initialValue: T,
    private val expirationDuration: Duration
) : MutableCacheImpl<T>(value = initialValue, expirationDate = LocalDateTime.now().plus(expirationDuration)), RefreshableCache<T> {

    override fun refresh(value: T) {
        this.value = value
        this.expirationDate = LocalDateTime.now().plus(expirationDuration)
    }

}

private class RefreshingCache<T>(private val expirationDuration: Duration, private val refresh: () -> T) : RefreshableCache<T> {

    private val cache: RefreshableCache<T> by lazy {
        RefreshableCacheImpl(initialValue = refresh(), expirationDuration)
    }

    override fun get(): T {
        if (cache.hasExpired()) refresh(value = refresh())
        return cache.get()
    }

    override fun refresh(value: T) {
        cache.refresh(value)
    }

    override fun invalidate() {
        cache.invalidate()
    }

    override fun isValid(): Boolean {
        return cache.isValid()
    }

    override val expirationDate: LocalDateTime get() = cache.expirationDate

}
