package at.mindcloud.cache

import java.time.Duration
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.ConcurrentHashMap

fun <K, T> keyValueCache(defaultExpirationDuration: Duration? = null): KeyValueCache<K, T> {
    return KeyValueCacheImpl(defaultExpirationDuration, cache = mutableMapOf())
}

fun <K, T> weakKeyValueCache(defaultExpirationDuration: Duration? = null): KeyValueCache<K, T> {
    return KeyValueCacheImpl(defaultExpirationDuration, cache = WeakHashMap())
}

fun <K, T> concurrentKeyValueCache(defaultExpirationDuration: Duration? = null): KeyValueCache<K, T> {
    return KeyValueCacheImpl(defaultExpirationDuration, cache = ConcurrentHashMap())
}

interface KeyValueCache<K, V> {

    val defaultExpirationDuration: Duration?

    fun add(key: K, value: V?, expirationDuration: Duration? = defaultExpirationDuration)

    operator fun get(key: K): V?

    fun invalidate(key: K)

    fun clear()

    fun clearExpired()
}


private class KeyValueCacheImpl<K, T>(
    override val defaultExpirationDuration: Duration?,
    private val cache: MutableMap<K, CachedElement<T>>,
) : KeyValueCache<K, T> {

    override fun add(key: K, value: T?, expirationDuration: Duration?) {
        cache[key] = CachedElement(
            value = value,
            expirationDate = if (expirationDuration != null) LocalDateTime.now().plus(expirationDuration) else null,
        )
    }

    override fun get(key: K): T? {
        val cachedElement = cache[key]
        return when {
            cachedElement == null -> null
            cachedElement.hasExpired() -> {
                cache.remove(key)
                null
            }
            else -> cachedElement.value
        }
    }

    override fun invalidate(key: K) {
        cache.remove(key)
    }

    override fun clear() {
        cache.clear()
    }

    override fun clearExpired() {
        cache.entries.filter { it.value.hasExpired() }.forEach { cache.remove(it.key) }
    }

    data class CachedElement<T>(val value: T?, val expirationDate: LocalDateTime?) {

        fun hasExpired(): Boolean {
            return expirationDate != null && LocalDateTime.now().isAfter(expirationDate)
        }

    }

}

