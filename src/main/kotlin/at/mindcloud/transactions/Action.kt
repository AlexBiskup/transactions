package at.mindcloud.transactions

interface Action<A> {

    val name: String? get() = null

    fun execute(): A

    fun revert(context: RevertContext<A>)

}
