package at.mindcloud.transactions

import at.mindcloud.transactions.Transaction.Status.*

fun <T> transactional(block: TransactionScope<T>.() -> T): T {
    return transaction(block).execute()
}

fun <T> transaction(block: TransactionScope<T>.() -> T): Transaction<T> {
    return TransactionImpl(block)
}

private class TransactionImpl<T>(private val block: TransactionScope<T>.() -> T) : Transaction<T>, TransactionScope<T> {

    override var status = DEFAULT
        private set

    private val actions = mutableListOf<StatefulAction<*>>()

    override fun execute(): T {
        check(isEligibleForExecution)
        return try {
            executeInternally()
        } catch (exception: Throwable) {
            failAndRollback(exception)
        }
    }

    override fun rollback(cause: Throwable?) {
        check(isEligibleForRollback)
        revertCompletedActions(cause)
        if (rollbackHasFailed()) throwRollbackFailedException(cause)
    }

    override fun <A> action(name: String?, revert: RevertContext<A>.() -> Unit, execute: () -> A): A {
        return action(ActionImpl(name = name ?: "${actions.size}", execute, revert))
    }

    override fun <A> action(action: Action<A>): A {
        val statefulAction =
            StatefulAction(name = action.name ?: "${actions.size}", StatefulAction.Status.DEFAULT, action)
        actions.add(statefulAction)
        return statefulAction.execute()
    }

    private fun executeInternally(): T {
        val result = block(this)
        status = COMPLETED
        return result
    }

    private fun failAndRollback(exception: Throwable): Nothing {
        status = FAILED
        rollback(cause = exception)
        throw exception
    }

    private fun throwRollbackFailedException(cause: Throwable?): Nothing {
        val affectedActions = actions.filter { it.status == StatefulAction.Status.REVERT_FAILED }
        val revertExceptions = affectedActions.mapNotNull { it.revertException }
        throw RollbackFailedException(revertExceptions, cause)
    }

    private fun rollbackHasFailed(): Boolean {
        resolveStatusAfterReverting()
        return status == ROLLBACK_FAILED
    }

    private fun resolveStatusAfterReverting() {
        status = when {
            actions.none { it.status == StatefulAction.Status.REVERT_FAILED } -> ROLLED_BACK
            else -> ROLLBACK_FAILED
        }
    }

    private fun revertCompletedActions(cause: Throwable?) {
        actions.filter { it.status == StatefulAction.Status.COMPLETED }
            .reversed()
            .forEach { it.revert(cause) }
    }

    private val isEligibleForRollback: Boolean
        get() {
            return status == COMPLETED || status == FAILED
        }

    private val isEligibleForExecution: Boolean
        get() {
            return status == DEFAULT
        }

}

class RollbackFailedException(revertExceptions: List<RevertFailedException>, cause: Throwable?) :
    IllegalStateException(cause) {

    init {
        for (exception in revertExceptions) addSuppressed(exception)
    }

    override val message =
        "Transactional integrity violated. Reverting actions ${revertExceptions.map { it.actionName }} failed."

}

class RevertFailedException(val actionName: String, override val cause: Throwable) :
    RuntimeException("Reverting action '$actionName' failed.")

class StatefulAction<A>(private val name: String, status: Status, private val delegate: Action<A>) {

    var status: Status = status
        private set

    var revertException: RevertFailedException? = null
        private set

    private var executeResult: Result<A>? = null

    fun execute(): A {
        check(status == Status.DEFAULT)
        return try {
            val result = delegate.execute()
            executeResult = Result(result)
            status = Status.COMPLETED
            result
        } catch (exception: Exception) {
            status = Status.ERROR
            throw exception
        }
    }

    fun revert(cause: Throwable?) {
        check(status == Status.COMPLETED)
        status = try {
            delegate.revert(RevertContextImpl(cause))
            Status.REVERTED
        } catch (exception: Throwable) {
            revertException = RevertFailedException(actionName = name, cause = exception)
            Status.REVERT_FAILED
        }
    }

    private inner class RevertContextImpl(override val exception: Throwable?) : RevertContext<A> {
        override val result: A
            get() = executeResult!!.value
    }

    private data class Result<T>(val value: T)

    enum class Status {
        DEFAULT, COMPLETED, ERROR, REVERTED, REVERT_FAILED
    }

}


private class ActionImpl<A>(
    override val name: String,
    private val execute: () -> A,
    private val revert: RevertContext<A>.() -> Unit,
) : Action<A> {

    override fun execute(): A {
        return execute.invoke()
    }

    override fun revert(context: RevertContext<A>) {
        revert.invoke(context)
    }

}
