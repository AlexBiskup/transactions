package at.mindcloud.transactions

fun <R> withResult(block: (result: R) -> Unit): RevertContext<R>.() -> Unit {
    return {
        block(result)
    }
}
