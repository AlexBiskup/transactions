package at.mindcloud.transactions

interface TransactionScope<T> {

    fun <A> action(name: String? = null, revert: RevertContext<A>.() -> Unit, execute: () -> A): A

    fun <A> action(action: Action<A>): A

}
