package at.mindcloud.transactions

interface RevertContext<A> {

    val result: A

    val exception: Throwable?

}
