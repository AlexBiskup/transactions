package at.mindcloud.transactions

interface Transaction<T> {

    val status: Status

    fun execute(): T

    fun rollback(cause: Throwable? = null)

    enum class Status {
        DEFAULT, COMPLETED, FAILED, ROLLED_BACK, ROLLBACK_FAILED
    }

}
