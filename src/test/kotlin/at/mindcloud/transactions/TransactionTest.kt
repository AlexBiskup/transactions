package at.mindcloud.transactions

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class TransactionTest {

    @Test
    fun transaction_withNoInteractions_hasStatusDefault() {
        val transaction = transaction<Unit> { }
        assertEquals(Transaction.Status.DEFAULT, transaction.status)
    }

    @Test
    fun transaction_executedWithNoErrors_hasStatusCompleted() {
        val transaction = transaction<Unit> { }
        transaction.execute()
        assertEquals(Transaction.Status.COMPLETED, transaction.status)
    }

    @Test
    fun transaction_rolledBackSuccessfully_hasStatusRolledBack() {
        val transaction = transaction<Unit> {
        }
        transaction.execute()
        transaction.rollback()
        assertEquals(Transaction.Status.ROLLED_BACK, transaction.status)
    }

    @Test
    fun transaction_withErrorOnRollback_hasStatusRollbackFailed() {
        val transaction = transaction<Unit> {
            action(revert = { throw IllegalArgumentException() }) {
            }
        }
        transaction.execute()
        try {
            transaction.rollback()
        } catch (exception: RollbackFailedException) {
        }
        assertEquals(Transaction.Status.ROLLBACK_FAILED, transaction.status)
    }

    @Test
    fun execute_withoutErrors_returnsCorrectResult() {
        val transaction = transaction<String> {
            "result"
        }
        val result = transaction.execute()
        assertEquals("result", result)
    }

    @Test
    fun execute_withErrorOutsideAction_throwsException() {
        val transaction = transaction<Unit> {
            throw IllegalAccessError()
        }
        assertThrows<IllegalAccessError> {
            transaction.execute()
        }
    }

    @Test
    fun execute_withErrorInsideAction_throwsException() {
        val transaction = transaction<Unit> {
            action(revert = {}) {
                throw IllegalAccessError()
            }
        }
        assertThrows<IllegalAccessError> {
            transaction.execute()
        }
    }

    @Test
    fun execute_executesAllActions() {
        val state = mutableMapOf(
            "a" to false,
            "b" to false,
            "c" to false,
        )
        val transaction = transaction<Unit> {
            action(revert = {}) {
                state["a"] = true
            }
            action(revert = {}) {
                state["b"] = true
            }
            action(revert = {}) {
                state["c"] = true
            }
        }
        transaction.execute()
        val expected = mutableMapOf(
            "a" to true,
            "b" to true,
            "c" to true,
        )
        assertEquals(expected, state)
    }


    @Test
    fun execute_chainsResultsCorrectly() {
        val transaction = transaction<String> {
            val resultA = action(revert = {}) {
                "a"
            }
            val resultB = action(revert = {}) {
                "$resultA-b"
            }
            action(revert = {}) {
                "$resultB-c"
            }
        }
        val result = transaction.execute()
        assertEquals("a-b-c", result)
    }

    @Test
    fun execute_withError_setsRolledBackStatus() {
        val transaction = transaction<Unit> {
            throw IllegalAccessError()
        }
        try {
            transaction.execute()
        } catch (exception: IllegalAccessError) {
        }
        assertEquals(Transaction.Status.ROLLED_BACK, transaction.status)
    }

    @Test
    fun execute_withError_revertsAllExecutedActions() {
        val state = mutableMapOf(
            "a" to false,
            "b" to false,
            "c" to false,
        )

        val transaction = transaction<Unit> {
            action(revert = { state["a"] = true }) {
            }
            action(revert = { state["b"] = true }) {
            }
            action(revert = { state["c"] = true }) {
                throw IllegalStateException()
            }
        }
        try {
            transaction.execute()
        } catch (exception: IllegalStateException) {
        }
        val expected = mutableMapOf(
            "a" to true,
            "b" to true,
            "c" to false,
        )
        assertEquals(expected, state)
    }

    @Test
    fun execute_withError_revertsActionsInReversedOrder() {
        var revertPosition = 0
        val state = mutableMapOf<String, Int>()
        val transaction = transaction<Unit> {
            action(revert = { state["a"] = revertPosition++ }) {
            }
            action(revert = { state["b"] = revertPosition++ }) {
            }
            action(revert = { state["c"] = revertPosition++ }) {
            }
            action(revert = { state["d"] = revertPosition++ }) {
                throw IllegalStateException()
            }
        }
        try {
            transaction.execute()
        } catch (exception: IllegalStateException) {
        }
        val expected = mutableMapOf(
            "a" to 2,
            "b" to 1,
            "c" to 0,
        )
        assertEquals(expected, state)
    }

    @Test
    fun execute_withError_callsRevertWithCorrectException() {
        val cause = IllegalArgumentException()
        var capturedCause: Throwable? = null
        val transaction = transaction<Unit> {
            action(revert = { capturedCause = this.exception }) {
            }
            action(revert = { }) {
                throw cause
            }
        }
        try {
            transaction.execute()
        } catch (exception: IllegalArgumentException) {
        }
        assertEquals(cause, capturedCause)
    }

    @Test
    fun execute_withError_callsRevertWithCorrectResult() {
        val state = mutableMapOf<String, String>()
        val transaction = transaction<Unit> {
            action(revert = { state["a"] = this.result }) {
                "action_a_result"
            }
            action(revert = { state["b"] = this.result }) {
                "action_b_result"
            }
            action(revert = {}) {
                throw IllegalArgumentException()
            }
        }
        try {
            transaction.execute()
        } catch (exception: IllegalArgumentException) {
        }
        val expected = mutableMapOf(
            "a" to "action_a_result",
            "b" to "action_b_result",
        )
        assertEquals(expected, state)
    }

    @Test
    fun rollback_revertsAllActions() {
        val state = mutableMapOf(
            "a" to false,
            "b" to false,
            "c" to false,
        )
        val transaction = transaction<Unit> {
            action(revert = { state["a"] = true }) {
            }
            action(revert = { state["b"] = true }) {
            }
            action(revert = { state["c"] = true }) {
            }
        }
        try {
            transaction.execute()
            transaction.rollback()
        } catch (exception: IllegalStateException) {
        }
        val expected = mutableMapOf(
            "a" to true,
            "b" to true,
            "c" to true,
        )
        assertEquals(expected, state)
    }

    @Test
    fun rollback_callsRevertWithCorrectCauseOnExceptions() {
        val cause = IllegalArgumentException()
        var capturedCause: Throwable? = null
        val transaction = transaction<Unit> {
            action(revert = { capturedCause = this.exception }) {
            }
        }
        transaction.execute()
        transaction.rollback(cause = cause)
        assertEquals(cause, capturedCause)
    }

    @Test
    fun rollback_callsRevertWithCorrectResult() {
        var capturesResult: String? = null
        val transaction = transaction<String> {
            action(revert = { capturesResult = this.result }) {
                "action_result"
            }
        }
        transaction.execute()
        transaction.rollback()
        assertEquals("action_result", capturesResult)
    }


    @Test
    fun rollback_withErrorOnRevert_throwsRollbackFailedException() {
        val transaction = transaction<Unit> {
            action(revert = { throw IllegalArgumentException() }) {
            }
        }
        transaction.execute()
        assertThrows<RollbackFailedException> {
            transaction.rollback()
        }
    }

    @Test
    fun rollback_withErrorOnRevert_revertsAllOtherActions() {
        val state = mutableMapOf(
            "a" to false,
            "b" to false,
            "c" to false,
        )
        val transaction = transaction<Unit> {
            action(revert = { state["a"] = true }) {
            }
            action(revert = { throw IllegalArgumentException() }) {
            }
            action(revert = { state["c"] = true }) {
            }
        }
        transaction.execute()
        try {
            transaction.rollback()
        } catch (exception: RollbackFailedException) {
        }
        val expected = mutableMapOf(
            "a" to true,
            "b" to false,
            "c" to true,
        )
        assertEquals(expected, state)
    }

}
