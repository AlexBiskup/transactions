package at.mindcloud.cache

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import java.time.Duration

internal class KeyValueCacheImplTest {

    @Test
    fun get_withValidValue_returnsCachedValue() {
        val cache = keyValueCache<String, String>()
        cache.add("key", "TestValue", expirationDuration = Duration.ofMillis(100))
        Thread.sleep(5)
        Assertions.assertEquals("TestValue", cache["key"])
    }

    @Test
    fun get_withExpiredValue_returnsNull() {
        val cache = keyValueCache<String, String>()
        cache.add("key", "TestValue", expirationDuration = Duration.ofMillis(10))
        Thread.sleep(11)
        Assertions.assertNull(cache["key"])
    }

    @Test
    fun add_withoutDuration_usesDefaultDuration() {
        val cache = keyValueCache<String, String>(defaultExpirationDuration = Duration.ofMillis(10))
        cache.add("key", "TestValue")
        Thread.sleep(11)
        Assertions.assertNull(cache["key"])
    }

    @Test
    fun addMultipleValues_storesValueWithCorrectKeys() {
        val cache = keyValueCache<String, String>()
        cache.add("key1", "TestValue1")
        cache.add("key2", "TestValue2")
        cache.add("key3", "TestValue3")
        Assertions.assertEquals("TestValue2", cache["key2"])
    }

    @Test
    fun add_withoutDuration_doesNotExpire() {
        val cache = keyValueCache<String, String>(defaultExpirationDuration = Duration.ofMillis(10))
        cache.add("key", "TestValue", expirationDuration = null)
        Thread.sleep(11)
        Assertions.assertNotNull(cache["key"])
    }

    @Test
    fun add_withoutDuration_overwritesDefault() {
        val cache = keyValueCache<String, String>(defaultExpirationDuration = Duration.ofMillis(10))
        cache.add("key", "TestValue", expirationDuration = Duration.ofMillis(20))
        Thread.sleep(10)
        Assertions.assertNotNull(cache["key"])
    }

    @Test
    fun defaultExpirationDuration_equalsConfiguredValue() {
        val cache = keyValueCache<String, String>(defaultExpirationDuration = Duration.ofMillis(10))
        Assertions.assertEquals(Duration.ofMillis(10), cache.defaultExpirationDuration)
    }

    @Test
    fun invalidate_expiresValueImmediately() {
        val cache = keyValueCache<String, String>()
        cache.add("key", "TestValue", expirationDuration = Duration.ofMillis(20))
        Thread.sleep(5)
        cache.invalidate("key")
        Assertions.assertNull(cache["key"])
    }

}
