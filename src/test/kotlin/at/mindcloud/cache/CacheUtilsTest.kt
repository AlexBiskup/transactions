package at.mindcloud.cache

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import java.time.Duration
import java.time.LocalDateTime

internal class CacheUtilsTest {

    @Test
    fun get_withValidExpirationDate_returnsValue() {
        val cache = cache("Value", expirationDate = LocalDateTime.now().plusMinutes(1))
        assertEquals("Value", cache.get())
    }

    @Test
    fun get_withExpiredExpirationDate_throws() {
        val cache = cache("Value", expirationDate = LocalDateTime.now().plus(Duration.ofMillis(10)))
        Thread.sleep(11)
        assertThrows<InvalidCacheException> {
            cache.get()
        }
    }

    @Test
    fun get_withValidExpirationDuration_returnsValue() {
        val cache = cache("Value", expirationDuration = Duration.ofMillis(10))
        assertEquals("Value", cache.get())
    }

    @Test
    fun get_withExpiredExpirationDuration_throws() {
        val cache = cache("Value", expirationDuration = Duration.ofMillis(10))
        Thread.sleep(11)
        assertThrows<InvalidCacheException> {
            cache.get()
        }
    }

    @Test
    fun invalidate_expiresValueImmediately() {
        val cache = cache("Value", expirationDuration = Duration.ofMillis(100))
        cache.invalidate()
        assertThrows<InvalidCacheException> {
            cache.get()
        }
    }

    @Test
    fun isValid_withValidCache_returnsTrue() {
        val cache = cache("Value", expirationDuration = Duration.ofMillis(100))
        assertTrue(cache.isValid())
        assertFalse(cache.hasExpired())
    }

    @Test
    fun isValid_withExpiredCache_returnsFalse() {
        val cache = cache("Value", expirationDuration = Duration.ofMillis(100))
        cache.invalidate()
        assertFalse(cache.isValid())
        assertTrue(cache.hasExpired())
    }

    @Test
    fun refresh_prolongsValidity() {
        val cache = refreshableCache("Value", expirationDuration = Duration.ofMillis(5))
        Thread.sleep(3)
        cache.refresh("NewValue")
        Thread.sleep(3)
        assertEquals("NewValue", cache.get())
    }

    @Test
    fun refreshingCache_initializesLazily() {
        var invocations = 0
        val cache = refreshingCache(expirationDuration = Duration.ofMillis(10)) {
            invocations += 1
            "Value"
        }
        Thread.sleep(5)
        assertEquals(0, invocations)
        cache.get()
        Thread.sleep(5)
        assertEquals("Value", cache.get())
        assertEquals(1, invocations)
    }

    @Test
    fun refreshingCache_refreshesAutomatically() {
        var invocations = 0
        val cache = refreshingCache(expirationDuration = Duration.ofMillis(10)) {
            invocations += 1
            "Value"
        }
        cache.get()
        Thread.sleep(10)
        assertEquals("Value", cache.get())
        assertEquals(2, invocations)
    }

}
