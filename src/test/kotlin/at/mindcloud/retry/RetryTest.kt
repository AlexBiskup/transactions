package at.mindcloud.retry

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class RetryTest {

    @Test
    fun attemptEqualsActualInvocation() {
        var invocation = 0
        retry(maxAttempts = 10) {
            invocation += 1
            assertEquals(invocation, attempt)
            if (attempt < 10) throw RuntimeException()
        }
    }

    @Test
    fun retry_withoutException_returnsResultOnFirstAttempt() {
        val attempts = retry {
            attempt
        }
        assertEquals(1, attempts)
    }

    @Test
    fun retry_withExceptionOnFirstAttempt_returnsResultOnSecondAttempt() {
        val attempts = retry {
            if (attempt == 2) attempt else throw RuntimeException()
        }
        assertEquals(2, attempts)
    }

    @Test
    fun retry_withExceptionOnSecondAttempt_throwsException() {
        assertThrows<IllegalArgumentException> {
            retry {
                if (attempt < 3) throw IllegalArgumentException()
            }
        }
    }

    @Test
    fun retry_withExceptionBeforeLastAttempt_returnsResultOnNextAttempt() {
        val attempts = retry(maxAttempts = 7) {
            if (attempt < 3) throw RuntimeException() else attempt
        }
        assertEquals(3, attempts)
    }

    @Test
    fun retry_withExceptionOneBeforeLastAttempt_returnsResultLastAttempt() {
        val attempts = retry(maxAttempts = 7) {
            if (attempt < 7) throw RuntimeException() else attempt
        }
        assertEquals(7, attempts)
    }

    @Test
    fun retry_withExceptionOnLastAttempt_throwsException() {
        assertThrows<IllegalStateException> {
            retry(maxAttempts = 7) {
                if (attempt < 8) throw IllegalStateException()
            }
        }
    }

}
